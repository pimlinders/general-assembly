from flask import Flask, request
from flask.ext.jsonpify import jsonify
import twitter
import os


app = Flask(__name__)

# set credentials
config = {
    'consumer_key': '9CaCgzhMRLZH0Kac9M2R3brSk',
    'consumer_secret':'iGY33qthmfOhcfWMLCm3xo6YGKi63ooUFqz80sIcecQOO08Sx1',
    'access_token_key': '14648629-Sl7ipX5bzQx41XUt2MUvVRSVmEjqb0E7hIIMOSMH8',
    'access_token_secret': '8QdinLTkgEeUlI4NMTd6hM4q7YKogODSwKcqDddEaRR5o'
}

@app.route('/search', methods=['GET'])
def search():
    response = {}
    term = None
    geocode = None
    # check if there is a search value available, if value is missing there's nothing to search
    if 'q' in request.values:
        term = request.values['q']
    # check if geo values are available
    if 'lat' in request.values and 'lng' in request.values and 'distance' in request.values:
        # set geocode to do a twitter geocode search
        geocode = (float(request.values['lat']), float(request.values['lng']), request.values['distance'])
    if term is not None or geocode is not None:
        # get tweets
        response = get_tweets(term, geocode)
    # convert python dict to json
    return jsonify(response)


def get_tweets(term, geocode=None):
    # initiate twitter api
    api = twitter.Api(
        consumer_key=config['consumer_key'],
        consumer_secret=config['consumer_secret'],
        access_token_key=config['access_token_key'],
        access_token_secret=config['access_token_secret']
    )
    # search the twitter api, https://dev.twitter.com/docs/api/1.1/get/search/tweets
    results = api.GetSearch(term=term, geocode=geocode, count=100, result_type="recent")
    # extract id, text, geo and username from the twitter results
    data = []
    for result in results:
        if geocode is None or (geocode is not None and result.GetGeo() is not None):
            data.append({
                'id': result.GetId(),
                'geo': result.GetGeo(),
                'text': result.GetText().encode('ascii', 'ignore'),
                'username': result.user.GetScreenName()
            })
    # return tweets
    return {
        'tweets': data
    }

if __name__ == "__main__":
    app.debug = True
    port = int(os.environ.get("PORT", 5000))
    app.run(host='0.0.0.0', port=port)
